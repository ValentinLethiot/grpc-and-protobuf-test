package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"
	"strings"

	db "grpcProto/database"
	"grpcProto/proto"

	"google.golang.org/grpc"
)

var myDatabase = db.CreateDatabase()

type humanService struct {
	proto.UnimplementedHumanServiceServer
}

func (s *humanService) Kill(ctx context.Context, request *proto.Human) (*proto.StrMessage, error) {
	if !request.GetAlive() {
		return nil, fmt.Errorf("OMG he is already dead !!")
	}
	request.Alive = false
	str, err := myDatabase.ModifyHuman(request)
	if err != nil {
		return nil, err
	}
	return &proto.StrMessage{
		Message: str,
	}, nil
}

func (s *humanService) WhoAreYou(ctx context.Context, request *proto.Human) (*proto.StrMessage, error) {
	str := fmt.Sprint("Your name is ", request.GetName(), " and you are ", request.GetAge(), " years old.")
	if request.GetAlive() {
		str += " Of course, you're alive."
	} else {
		str += " And, OMGGGG A GHOST !!!!"
	}
	return &proto.StrMessage{
		Message: str,
	}, nil
}

func (s *humanService) CreateHuman(ctx context.Context, hm *proto.Human) (*proto.StrMessage, error) {
	ok, err := myDatabase.AddHuman(hm)
	if err != nil {
		return nil, err
	}
	return &proto.StrMessage{
		Message: ok,
	}, nil
}

func (s *humanService) GetHuman(ctx context.Context, msg *proto.StrMessage) (*proto.Human, error) {
	log.Println(msg)
	id, err := strconv.Atoi(msg.GetMessage())
	if err != nil {
		return nil, err
	}
	hm, err := myDatabase.GetHuman(id)
	if err != nil {
		return nil, err
	}
	return hm, nil
}

func (s *humanService) ModifyHuman(ctx context.Context, hm *proto.Human) (*proto.StrMessage, error) {
	msg, err := myDatabase.ModifyHuman(hm)
	if err != nil {
		return nil, err
	}
	return &proto.StrMessage{
		Message: msg,
	}, nil
}

func (s *humanService) DeleteHuman(ctx context.Context, msg *proto.StrMessage) (*proto.StrMessage, error) {
	id, err := strconv.Atoi(msg.GetMessage())
	if err != nil {
		return nil, err
	}
	msgReturned, err := myDatabase.DeleteHuman(id)
	if err != nil {
		return nil, err
	}
	return &proto.StrMessage{
		Message: msgReturned,
	}, nil
}

func main() {
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("Listen error : %v", err)
	}

	//create an instance of a grpc server and register our implementation
	s := grpc.NewServer()
	proto.RegisterHumanServiceServer(s, &humanService{})

	s.Serve(lis)
	/*conn, err := grpc.Dial("localhost:8081", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Dial error : %v", err)
	}
	defer conn.Close()

	r := runtime.NewServeMux()
	proto.RegisterHumanServiceHandler(context.Background(), r, conn)

	err = http.ListenAndServe(":8081", httpGrpcRouter(s, r))
	if err != nil {
		log.Fatalf("ListenAndServeError : %v", err)
	}*/
}

func httpGrpcRouter(grpcServer *grpc.Server, httpHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.ProtoMajor == 2 && strings.Contains(r.Header.Get("Content-Type"), "application/grpc") {
			fmt.Println("aaaaa")
			grpcServer.ServeHTTP(w, r)
		} else {
			fmt.Println(r.URL)
			httpHandler.ServeHTTP(w, r)
		}
	})
}

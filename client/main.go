package main

import (
	"context"
	"grpcProto/proto"
	"log"
	"time"

	"google.golang.org/grpc"
)

var human0 = proto.Human{
	Id:    0,
	Name:  "Joe",
	Age:   10,
	Sex:   true,
	Alive: true,
}
var human1 = proto.Human{
	Id:    1,
	Name:  "William",
	Age:   20,
	Sex:   false,
	Alive: true,
}
var human2 = proto.Human{
	Id:    2,
	Name:  "Jack",
	Age:   30,
	Sex:   true,
	Alive: true,
}
var human3 = proto.Human{
	Id:    3,
	Name:  "Averell",
	Age:   40,
	Sex:   false,
	Alive: true,
}

func sendWhoAreYou(myClient proto.HumanServiceClient, ctx context.Context, human proto.Human) {
	str, err := myClient.WhoAreYou(ctx, &human)
	if err != nil {
		log.Fatalf("error WhoAreYou : %v", err)
	}
	log.Println(str)
}

func sendKill(myClient proto.HumanServiceClient, ctx context.Context, human proto.Human) {
	str, err := myClient.Kill(ctx, &human)
	if err != nil {
		log.Fatalf("error Kill : %v", err)
	}
	log.Println(str)
}

func sendCreate(myClient proto.HumanServiceClient, ctx context.Context, human proto.Human) {
	str, err := myClient.CreateHuman(ctx, &human)
	if err != nil {
		log.Fatalf("error Create : %v", err)
	}
	log.Println(str.GetMessage())
}

func sendModify(myClient proto.HumanServiceClient, ctx context.Context, human proto.Human) {
	str, err := myClient.ModifyHuman(ctx, &human)
	if err != nil {
		log.Fatalf("error Modify : %v", err)
	}
	log.Println(str.GetMessage())
}

func sendGet(myClient proto.HumanServiceClient, ctx context.Context, id string) *proto.Human {
	msg := proto.StrMessage{
		Message: string(id),
	}
	human, err := myClient.GetHuman(ctx, &msg)

	if err != nil {
		log.Fatalf("error Get : %v", err)
	}
	return human
}

func sendDelete(myClient proto.HumanServiceClient, ctx context.Context, id string) {
	msg := proto.StrMessage{
		Message: string(id),
	}
	str, err := myClient.DeleteHuman(ctx, &msg)
	if err != nil {
		log.Fatalf("error Delete : %v", err)
	}
	log.Println(str.GetMessage())
}

func rempliDB(myClient proto.HumanServiceClient, ctx context.Context) {
	sendCreate(myClient, ctx, human0)
	sendCreate(myClient, ctx, human1)
	sendCreate(myClient, ctx, human2)
	sendCreate(myClient, ctx, human3)
}

func main() {
	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	myClient := proto.NewHumanServiceClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	rempliDB(myClient, ctx)
	//sendCreate(myClient, ctx, human2)
	//sendGet(myClient, ctx, "2")
	//sendDelete(myClient, ctx, "2")
	//sendGet(myClient, ctx, "2")
	/*hm := sendGet(myClient, ctx, "0")

	sendWhoAreYou(myClient, ctx, *hm)
	sendKill(myClient, ctx, *hm)

	hm = sendGet(myClient, ctx, "0")
	sendWhoAreYou(myClient, ctx, *hm)
	sendKill(myClient, ctx, *hm)*/
}

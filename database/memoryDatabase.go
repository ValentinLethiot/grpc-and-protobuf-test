package server

import (
	"fmt"
	"grpcProto/proto"
)

type Database struct {
	MyHumans []*proto.Human
}

func CreateDatabase() *Database {
	mhs := make([]*proto.Human, 0)
	return &Database{
		MyHumans: mhs,
	}
}

func (db *Database) AddHuman(hm *proto.Human) (string, error) {
	for i := 0; i < len(db.MyHumans); i++ {
		if db.MyHumans[i].GetId() == hm.GetId() {
			return "", fmt.Errorf("This human already exists")
		}
	}
	db.MyHumans = append(db.MyHumans, hm)
	return "This human has been added", nil
}

func (db *Database) DeleteHuman(id int) (string, error) {
	for i := 0; i < len(db.MyHumans); i++ {
		if db.MyHumans[i].GetId() == int64(id) {
			db.MyHumans = append(db.MyHumans[:i], db.MyHumans[i+1:]...)
			return "This human has deseapered", nil
		}
	}
	return "", fmt.Errorf("This human doesn't exists")
}

func (db *Database) GetHuman(id int) (*proto.Human, error) {
	for i := 0; i < len(db.MyHumans); i++ {
		if db.MyHumans[i].GetId() == int64(id) {
			return db.MyHumans[i], nil
		}
	}
	return nil, fmt.Errorf("This human doesn't exists")
}

func (db *Database) ModifyHuman(hm *proto.Human) (string, error) {
	for i := 0; i < len(db.MyHumans); i++ {
		if db.MyHumans[i].GetId() == int64(hm.GetId()) {
			db.MyHumans[i] = hm
			return "This human has been modified", nil
		}
	}
	return "", fmt.Errorf("This human doesn't exists")
}

module grpcProto

go 1.12

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/golang/protobuf v1.3.2
	github.com/grpc-ecosystem/grpc-gateway v1.11.3
	github.com/micro/protobuf v0.0.0-20180321161605-ebd3be6d4fdb // indirect
	github.com/philips/go-bindata-assetfs v0.0.0-20150624150248-3dcc96556217 // indirect
	github.com/philips/grpc-gateway-example v0.0.0-20170619012617-a269bcb5931c // indirect
	github.com/spf13/cobra v0.0.5 // indirect
	google.golang.org/genproto v0.0.0-20180817151627-c66870c02cf8
	google.golang.org/grpc v1.24.0
)
